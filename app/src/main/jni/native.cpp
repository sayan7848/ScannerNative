//
// Created by sayan on 26/6/16.
//

#include <opencv2/opencv.hpp>
#include <android/log.h>
#include "../java/opencv/horntail/com/scannernative/opencvnativehelper/opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper.h"
#define TAG "native"

using namespace cv;
using namespace std;

void enhanceChannel(Mat &image,Mat &imagetoenhance,Mat &mask)
{
    Mat channel(image.rows,image.cols,CV_8UC1);
    image.copyTo(channel,mask);
    cvtColor(channel,channel,CV_RGB2GRAY,1);
    equalizeHist(channel,channel);
    cvtColor(channel,channel,CV_GRAY2RGB,3);
    channel.copyTo(imagetoenhance,mask);
    channel.release();
    channel = NULL;
}
void histEqualizeRGB(Mat &image,Mat &result)
{
    __try {
        Mat redEnhanced,blueEnhanced,greenEnhanced;
        image.copyTo(redEnhanced);
        Scalar scalar(1, 0, 0);
        Mat redMask(image.rows, image.cols, image.type(), scalar);
        enhanceChannel(image, redEnhanced, redMask);
        redEnhanced.copyTo(blueEnhanced);
        Scalar scalar1(0,0,1);
        Mat blueMask(image.rows,image.cols,image.type(),scalar1);
        enhanceChannel(image,blueEnhanced,blueMask);
        blueEnhanced.copyTo(greenEnhanced);
        Scalar scalar2(0,1,0);
        Mat greenMask(image.rows,image.cols,image.type(),scalar2);
        enhanceChannel(image,greenEnhanced,greenMask);
        greenEnhanced.copyTo(result);
        redEnhanced.release();
        blueEnhanced.release();
        greenEnhanced.release();
        redMask.release();
        blueMask.release();
        greenMask.release();
        greenEnhanced = NULL;
        redEnhanced = NULL;
        blueEnhanced = NULL;
        redMask = NULL;
        greenMask = NULL;
        blueMask = NULL;
    }
    catch (Exception ex)
    {
        __android_log_print(ANDROID_LOG_ERROR,TAG,"error occured at histEqualizeRGB");
    }
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_bgr2rgb
        (JNIEnv *env, jobject thiz, jlong ptr1, jlong ptr2)
{
    Mat* m1 = (Mat*)ptr1;
    Mat* m2 = (Mat*)ptr2;
    cvtColor(*m1,*m2,CV_BGR2RGB);
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_resize(JNIEnv *env,jobject thiz,jlong ptr1,jlong ptr2,jdouble factor)
{
    Mat* m1 = (Mat*)ptr1;
    Mat* m2 = (Mat*)ptr2;
    Size dsize;
    resize(*m1,*m2,dsize,factor,factor,INTER_AREA);
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_x2gray
        (JNIEnv *env, jobject thiz, jlong ptr1, jlong ptr2)
{
    Mat* mat1 = (Mat*)ptr1;
    Mat* mat2 = (Mat*)ptr2;
    if((*mat1).type() == CV_8UC3)
    {
        cvtColor(*mat1,*mat2,CV_RGB2GRAY);

    }
    if((*mat1).type() == CV_8UC1)
    {
        (*mat1).copyTo(*mat2);
    }
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_x2rgb
        (JNIEnv *env, jobject thiz, jlong ptr1, jlong ptr2)
{
    Mat* mat1 = (Mat*)ptr1;
    Mat* mat2 = (Mat*)ptr2;
    if((*mat1).type() == CV_8UC1)
    {
        cvtColor(*mat1,*mat2,CV_GRAY2RGB);
        __android_log_print(ANDROID_LOG_ERROR,TAG,"2rgb for cvtype c1 called");
    }
    if((*mat1).type() == CV_8UC3){
        __android_log_print(ANDROID_LOG_ERROR, TAG, "test int = %d", (*mat2).type());
        (*mat1).copyTo(*mat2);
    }
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_sobel
        (JNIEnv *env, jobject thiz, jlong ptr1, jlong ptr2,jint kernelSize)
{
    Mat* mat1 = (Mat*)ptr1;
    Mat* mat2 = (Mat*)ptr2;
    Mat blurredImage,greyImage;
    Size size(kernelSize,kernelSize);
    GaussianBlur(*mat1,blurredImage,size,0,0);
    if(blurredImage.type() == CV_8UC3)
        cvtColor(blurredImage,greyImage,CV_RGB2GRAY);
    else if(blurredImage.type() == CV_8UC1)
        blurredImage.copyTo(greyImage);
    else{
        *mat2 = NULL;
        return;
    }
    Mat xFirstDerivative,yFirstDerivative,absXD,absYD;
    int ddepth = CV_16S;
    Sobel(greyImage,xFirstDerivative,ddepth,1,0,kernelSize);
    Sobel(greyImage,yFirstDerivative,ddepth,0,1,kernelSize);
    convertScaleAbs(xFirstDerivative,absXD);
    convertScaleAbs(yFirstDerivative,absYD);
    addWeighted(absXD,0.5,absYD,0.5,0,*mat2);
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_canny
        (JNIEnv *env, jobject thiz, jlong ptr1, jlong ptr2,jint kernelSize)
{
    Mat* mat1 = (Mat*)ptr1;
    Mat* mat2 = (Mat*)ptr2;
    Mat greyImage;
    if((*mat1).type() == CV_8UC3)
        cvtColor(*mat1,greyImage,CV_RGB2GRAY);
    else if((*mat1).type() == CV_8UC1)
        (*mat1).copyTo(greyImage);
    else{
        *mat2 = NULL;
        return;
    }
    Canny(greyImage,*mat2,50,150,kernelSize);
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_histEqual
        (JNIEnv *env, jobject thiz, jlong ptr1, jlong ptr2)
{
    Mat* mat1 = (Mat*)ptr1;
    Mat* mat2 = (Mat*)ptr2;
    if((*mat1).type() == CV_8UC3)
    {
        histEqualizeRGB(*mat1,*mat2);
    }
    if((*mat1).type() == CV_8UC1)
    {
        equalizeHist(*mat1,*mat2);
    }
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_drawHist
        (JNIEnv *env, jobject thiz, jlong Matptr1,jlong Matptr2)
{
    Mat* mat1 = (Mat*)Matptr1;
    Mat* mat2 = (Mat*)Matptr2;
    if((*mat1).type() == CV_8UC1)
        cvtColor(*mat1,*mat2,CV_GRAY2RGB);
    else
        (*mat1).copyTo(*mat2);
    vector<Mat> rgb_planes;
    split(*mat2,rgb_planes);
    int histSize = 256;
    float range[] = { 0, 256 } ;
    const float* histRange = { range };

    bool uniform = true; bool accumulate = false;

    Mat b_hist, g_hist, r_hist;

    /// Compute the histograms:
    calcHist( &rgb_planes[0], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate );
    calcHist( &rgb_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate );
    calcHist( &rgb_planes[2], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate );

    // Draw the histograms for B, G and R
    int hist_w = 512; int hist_h = 400;
    int bin_w = cvRound( (double) hist_w/histSize );

    Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0) );

    // Normalize the result to [ 0, histImage.rows ]
    normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );

    for( int i = 1; i < histSize; i++ )
    {
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(b_hist.at<float>(i-1)) ) ,
              Point( bin_w*(i), hist_h - cvRound(b_hist.at<float>(i)) ),
              Scalar( 255, 0, 0), 2, 8, 0  );
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(g_hist.at<float>(i-1)) ) ,
              Point( bin_w*(i), hist_h - cvRound(g_hist.at<float>(i)) ),
              Scalar( 0, 255, 0), 2, 8, 0  );
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(r_hist.at<float>(i-1)) ) ,
              Point( bin_w*(i), hist_h - cvRound(r_hist.at<float>(i)) ),
              Scalar( 0, 0, 255), 2, 8, 0  );
    }
    histImage.copyTo(*mat2);
    b_hist.release();
    g_hist.release();
    r_hist.release();
    histImage.release();
    b_hist = NULL;
    g_hist = NULL;
    r_hist = NULL;
    histImage = NULL;
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_averageFilter
        (JNIEnv *env, jobject thiz, jlong Matptr1, jlong Matptr2, jint kernelSize)
{
    Mat* mat1 = (Mat*)Matptr1;
    Mat* mat2 = (Mat*)Matptr2;
    Size size(kernelSize,kernelSize);
    blur(*mat1,*mat2,size);
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_medianFilter
        (JNIEnv *env, jobject thiz, jlong Matptr1, jlong Matptr2, jint kernelSize)
{
    Mat* mat1 = (Mat*)Matptr1;
    Mat* mat2 = (Mat*)Matptr2;
    medianBlur(*mat1,*mat2,kernelSize);
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_gaussianFilter
        (JNIEnv *env, jobject thiz, jlong Matptr1, jlong Matptr2, jint kernelSize)
{
    Mat* mat1 = (Mat*)Matptr1;
    Mat* mat2 = (Mat*)Matptr2;
    Size size(kernelSize,kernelSize);
    GaussianBlur(*mat1,*mat2,size,0);
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_getScannedImage
        (JNIEnv *env, jobject thiz, jlong mat1, jlong mat2, jlong srcmat, jlong dstmat)
{


    Mat& m1 = *(Mat*)mat1;
    Mat& m2 = *(Mat*)mat2;
    Mat& sm = *(Mat*)srcmat;
    Mat& dm = *(Mat*)dstmat;
    try {
        Mat transformation(getPerspectiveTransform(sm,dm));
        warpPerspective(m1,m2,transformation,m2.size());
    }
    catch (Exception ex)
    {
        __android_log_print(ANDROID_LOG_ERROR,TAG,"error while scanning");
        m2 = NULL;
    }
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_x2MagicColor
        (JNIEnv *env, jobject thiz, jlong ptr1, jlong ptr2)
{
    Mat& m1 = *(Mat*)ptr1;
    Mat& m2 = *(Mat*)ptr2;

    __try{
        m1.convertTo(m2,-1,1.9,-80);
    }
    catch (Exception ex)
    {
        __android_log_print(ANDROID_LOG_ERROR,TAG,"error while converting to magic colour");
        m2 = NULL;
    }
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_x2BW
        (JNIEnv *env, jobject thiz, jlong ptr1, jlong ptr2)
{
    Mat& m1 = *(Mat*)ptr1;
    Mat& m2 = *(Mat*)ptr2;

    __try{
        if(m1.type() == CV_8UC3)
        {
            cvtColor(m1,m2,CV_RGB2GRAY);
        }
        else
        {
            m2 = m1.clone();
        }
        adaptiveThreshold(m2,m2,255,ADAPTIVE_THRESH_GAUSSIAN_C,THRESH_BINARY,9,7);
    }
    catch (Exception ex)
    {
        __android_log_print(ANDROID_LOG_ERROR,TAG,"error while converting to magic colour");
        m2 = NULL;
    }
}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_houghLineTransform
        (JNIEnv *env, jobject thiz, jlong ptr1, jlong ptr2)
{
    Mat& mat1 = *(Mat*)ptr1;
    Mat& mat2 = *(Mat*)ptr2;
    try {
        Mat temp;
        if(mat1.type() == CV_8UC3)
            cvtColor(mat1,temp,CV_RGB2GRAY);
        else
            temp = mat1.clone();
        Canny(temp,temp,50,200);
        vector<Vec4i> lines;
        HoughLinesP(temp,lines,1,CV_PI/180,50,50,10);
        cvtColor(temp,temp,CV_GRAY2RGB);
        for( size_t i = 0; i < lines.size(); i++ )
        {
            Vec4i l = lines[i];
            line( temp, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 3);
        }
        mat2 = temp.clone();
        temp.release();
        temp = NULL;
    }
    catch (Exception ex)
    {
        __android_log_print(ANDROID_LOG_ERROR,TAG,"error in hough line transform");
        mat2 = NULL;
    }

}

JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_houghCircles
        (JNIEnv *env, jobject thiz, jlong ptr1, jlong ptr2) {
    Mat &src = *(Mat *) ptr1;
    Mat &dst = *(Mat *) ptr2;
    try {
        Mat src_gray;
        if (src.type() == CV_8UC3)
            cvtColor(src, src_gray, CV_RGB2GRAY);
        else
            src_gray = src.clone();
        /// Reduce the noise so we avoid false circle detection
        GaussianBlur(src_gray, src_gray, Size(9, 9), 2, 2);

        vector<Vec3f> circles;

        /// Apply the Hough Transform to find the circles
        HoughCircles(src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows / 8, 200, 100, 0, 0);

        /// Draw the circles detected
        for (size_t i = 0; i < circles.size(); i++) {
            Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
            int radius = cvRound(circles[i][2]);
            // circle center
            circle(src, center, 3, Scalar(0, 255, 0), -1, 8, 0);
            // circle outline
            circle(src, center, radius, Scalar(0, 0, 255), 3, 8, 0);
        }
        dst = src.clone();
        src_gray.release();
        src_gray = NULL;
    }
    catch (Exception ex) {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "error in hough circle transform");
        dst = NULL;
    }
}
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_rotate
        (JNIEnv *env, jobject thiz, jlong mat1, jlong mat2,jint code)
{
    Mat& ptr1 = *(Mat *)mat1;
    Mat& ptr2 = *(Mat *)mat2;
    transpose(ptr1,ptr2);
    flip(ptr2,ptr2,code);
}
