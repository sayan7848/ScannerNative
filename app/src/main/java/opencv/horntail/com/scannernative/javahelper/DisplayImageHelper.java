package opencv.horntail.com.scannernative.javahelper;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import org.opencv.android.Utils;
import org.opencv.core.Mat;


public class DisplayImageHelper {

    private final String TAG = "Display";

    public  boolean display(Mat image, ImageView view)
    {
        if(image != null && view != null)
        {
            Log.e("called","display");
            Bitmap bitMap = Bitmap.createBitmap(image.cols(), image.rows(), Bitmap.Config.RGB_565);
            Utils.matToBitmap(image, bitMap);
            view.setImageBitmap(bitMap);
            return true;
        }
        return false;
    }

    public  boolean display(Bitmap bmp, ImageView imageView)
    {
        try
        {
            imageView.setImageBitmap(bmp);
            return true;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            return false;
        }
    }
}
