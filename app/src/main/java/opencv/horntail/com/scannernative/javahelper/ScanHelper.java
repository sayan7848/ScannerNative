package opencv.horntail.com.scannernative.javahelper;

import android.graphics.PointF;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.utils.Converters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import opencv.horntail.com.scannernative.R;
import opencv.horntail.com.scannernative.opencvnativehelper.NativeHelper;
import opencv.horntail.com.scannernative.thirdpartyhelpers.PolygonView;


public class ScanHelper {

    private ImageView imageView;
    private AppCompatActivity appCompatActivity;
    private Mat sampledImage;
    private final String TAG = "ScanHelper";

    public ScanHelper(PolygonView polygonView, ImageView imageView, AppCompatActivity appCompatActivity, Mat sampledImage)
    {
        this.imageView = imageView;
        this.appCompatActivity = appCompatActivity;
        this.sampledImage = sampledImage;
        setUpPolygonView(polygonView);
    }

    private void setUpPolygonView(PolygonView polygonView)
    {
        polygonView.setPoints(polygonView.getOrderedPoints(pointList(imageView)));
        polygonView.setVisibility(View.VISIBLE);
    }

    private List<PointF> pointList(ImageView imageView)
    {
        float height = imageView.getHeight() - appCompatActivity.getResources().getDimension(R.dimen.activity_vertical_margin);
        float width = imageView.getWidth() - appCompatActivity.getResources().getDimension(R.dimen.activity_horizontal_margin);
        List<PointF> points = new ArrayList<>();
        points.add(new PointF(0,0));
        points.add(new PointF(width,0));
        points.add(new PointF(0,height));
        points.add(new PointF(width,height));
        return points;
    }

    public void disablePolygonView(PolygonView polygonView)
    {
        polygonView.setVisibility(View.GONE);
    }

    public Mat startScan(PolygonView polygonView)
    {
        try {
            Map<Integer, PointF> polygonViewPoints = polygonView.getPoints();
            if(!polygonView.isValidShape(polygonViewPoints))
                return null;
            List<Point> corners = new ArrayList<>();
            for (int i = 0; i < polygonViewPoints.size(); i++) {
                int porojectedX = (int) ((double) polygonViewPoints.get(i).x *
                        ((double) sampledImage.width() / (double) imageView.getWidth()));
                int porojectedY = (int) ((double) polygonViewPoints.get(i).y *
                        ((double) sampledImage.height() / (double) imageView.getHeight()));
                Point corner = new Point(porojectedX, porojectedY);
                corners.add(corner);
            }
            Mat correctedImage = new Mat(sampledImage.rows(), sampledImage.cols(), sampledImage.type());
            Mat srcPoints = Converters.vector_Point2f_to_Mat(corners);
            Mat destPoints = Converters.vector_Point2f_to_Mat(Arrays.asList(new Point[]{new Point(0, 0), new Point(correctedImage.cols(), 0), new Point(0, correctedImage.rows()), new Point(correctedImage.cols(), correctedImage.rows())}));
            NativeHelper nativeHelper = new NativeHelper();
            nativeHelper.getScannedImage(sampledImage.getNativeObjAddr(),correctedImage.getNativeObjAddr(),srcPoints.getNativeObjAddr(),destPoints.getNativeObjAddr());
            sampledImage.release();
            sampledImage = null;
            return correctedImage;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            return null;
        }
    }

    public Mat getBW(Mat sampledImage)
    {
        Mat result;
        try
        {
            result = new Mat();
            NativeHelper nativeHelper = new NativeHelper();
            nativeHelper.x2BW(sampledImage.getNativeObjAddr(),result.getNativeObjAddr());
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            result = null;
        }
        return result;
    }

    public Mat getMagicColour(Mat sampledImage)
    {
        Mat result;
        try
        {
            result = new Mat();
            NativeHelper nativeHelper = new NativeHelper();
            nativeHelper.x2MagicColor(sampledImage.getNativeObjAddr(),result.getNativeObjAddr());
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            result = null;
        }
        return result;
    }

    public Mat getGray(Mat sampledImage)
    {
        Mat result;
        try
        {
            result = new Mat();
            NativeHelper nativeHelper = new NativeHelper();
            nativeHelper.x2gray(sampledImage.getNativeObjAddr(),result.getNativeObjAddr());
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            result = null;
        }
        return result;
    }

}
