package opencv.horntail.com.scannernative.javahelper;

import android.util.Log;

import org.opencv.core.Mat;

import opencv.horntail.com.scannernative.opencvnativehelper.NativeHelper;

/**
 * Created by sayan on 29/6/16.
 */
public class HoughTransformHelper {

    private final String TAG = "HoughTransformHelper";

    public Mat lineTransform(Mat sampledImage)
    {
        Mat result = null;
        try
        {
            NativeHelper nativeHelper = new NativeHelper();
            result = new Mat();
            nativeHelper.houghLineTransform(sampledImage.getNativeObjAddr(),result.getNativeObjAddr());
            sampledImage.release();
            sampledImage = null;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            result = null;
        }
        return result;
    }

    public Mat circleTransform(Mat sampledImage)
    {
        Mat result = null;
        try
        {
            NativeHelper nativeHelper = new NativeHelper();
            result = new Mat();
            nativeHelper.houghCircles(sampledImage.getNativeObjAddr(),result.getNativeObjAddr());
            sampledImage.release();
            sampledImage = null;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            result = null;
        }
        return result;
    }
}
