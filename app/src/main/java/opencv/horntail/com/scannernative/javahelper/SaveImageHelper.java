package opencv.horntail.com.scannernative.javahelper;

import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;


public class SaveImageHelper {

    private final String TAG = "saveImageHelper";

    public  boolean saveImage(Bitmap bitmap, AppCompatActivity appCompatActivity)
    {
        try
        {
            File file = new File(Environment.getExternalStorageDirectory()+"/tie/savedPics");
            if(!file.isDirectory())
                file.mkdirs();
            file = new File(Environment.getExternalStorageDirectory() + "/tie/savedPics/pic" + "_" + System.currentTimeMillis() + ".jpg");
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                if(!bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream))
                    return false;
                else {

                    MediaScannerConnection.scanFile(appCompatActivity,
                            new String[] { file.toString() }, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.d(TAG,"scan completed");
                                }
                            });
                    return true;
                }
            }catch (Exception ex)
            {
                return false;
            }

        }
        catch (Exception ex) {
            return false;
        }
    }

}
