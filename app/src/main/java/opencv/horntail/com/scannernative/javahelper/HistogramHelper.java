package opencv.horntail.com.scannernative.javahelper;

import android.util.Log;

import org.opencv.core.Mat;

import opencv.horntail.com.scannernative.opencvnativehelper.NativeHelper;


public class HistogramHelper {

    private  final String TAG = "HistogramHelper";

    public Mat histEqualize(Mat sampledImage)
    {
        NativeHelper nativeHelper = new NativeHelper();
        try
        {
            Log.e(TAG, Long.toString(sampledImage.getNativeObjAddr()));
            Mat result = new Mat();
            Log.e(TAG, Long.toString(result.getNativeObjAddr()));
            nativeHelper.histEqual(sampledImage.getNativeObjAddr(),result.getNativeObjAddr());
            Log.e(TAG, Long.toString(result.getNativeObjAddr()));
            return result;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            return null;
        }
    }

    public Mat drawHist(Mat sampledImage)
    {
        NativeHelper nativeHelper = new NativeHelper();
        try
        {
            Mat result = new Mat();
            nativeHelper.drawHist(sampledImage.getNativeObjAddr(),result.getNativeObjAddr());
            return result;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            return null;
        }
    }
}
