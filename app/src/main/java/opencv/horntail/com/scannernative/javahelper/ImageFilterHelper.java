package opencv.horntail.com.scannernative.javahelper;

import android.util.Log;

import org.opencv.core.Mat;

import opencv.horntail.com.scannernative.opencvnativehelper.NativeHelper;


public class ImageFilterHelper {

    private  final String TAG = "ImageFilterHelper";

    public Mat averageFilter(Mat image, int kernelSize)
    {
        NativeHelper nativeHelper = new NativeHelper();
        try {
            Mat result = new Mat();
            nativeHelper.averageFilter(image.getNativeObjAddr(), result.getNativeObjAddr(), kernelSize);
            image.release();
            image = null;
            return result;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            return  null;
        }
    }

    public Mat medianFilter(Mat image, int kernelSize)
    {
        NativeHelper nativeHelper = new NativeHelper();
        try {
            Mat result = new Mat();
            nativeHelper.medianFilter(image.getNativeObjAddr(), result.getNativeObjAddr(), kernelSize);
            image.release();
            image = null;
            return result;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            return  null;
        }
    }

    public Mat gaussianFilter(Mat image, int kernelSize)
    {
        NativeHelper nativeHelper = new NativeHelper();
        try {
            Mat result = new Mat();
            nativeHelper.gaussianFilter(image.getNativeObjAddr(), result.getNativeObjAddr(), kernelSize);
            image.release();
            image = null;
            return result;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            return  null;
        }
    }
}
