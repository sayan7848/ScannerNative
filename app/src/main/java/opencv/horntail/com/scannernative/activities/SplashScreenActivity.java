package opencv.horntail.com.scannernative.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import opencv.horntail.com.scannernative.R;

import static android.os.Build.VERSION.SDK;
import static android.os.Build.VERSION.SDK_INT;

public class SplashScreenActivity extends AppCompatActivity {

    int delay;
    final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 1;
    final int MY_PERMISSIONS_REQUEST_DEVICE_CAMERA = 2;
    private final String TAG = "SplashScreenActivity";
    Runnable r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.hide();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        delay = 1500;

        r = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        };

        if(Build.VERSION.SDK_INT >= 23) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Write Permission needed to load and save images", Snackbar.LENGTH_SHORT).show();
                }
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
            } else {
                handler.sendEmptyMessage(0);
            }
        }
        else
        {
            handler.postDelayed(r, delay);
        }
    }

    private Handler handler = new Handler(Looper.getMainLooper())
    {
        @Override
        public void handleMessage(Message message)
        {
            switch (message.what)
            {
                case 0:
                    if(ContextCompat.checkSelfPermission(SplashScreenActivity.this,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                    {
                        if(ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,
                                Manifest.permission.CAMERA))
                        {
                            Snackbar.make(getWindow().getDecorView().getRootView(),"Camera Permission needed to take full advantage of app functionality",Snackbar.LENGTH_SHORT).show();
                        }
                        ActivityCompat.requestPermissions(SplashScreenActivity.this,new String[]{Manifest.permission.CAMERA},MY_PERMISSIONS_REQUEST_DEVICE_CAMERA);
                    }
                    else
                    {
                        handler.sendEmptyMessage(1);
                    }
                    break;
                case 1:
                    new Handler().postDelayed(r,delay);
                default:
                    break;
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions,int[] grantResults)
    {
        switch (requestCode)
        {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    handler.sendEmptyMessage(0);
                }
                else
                {
                    Snackbar.make(getWindow().getDecorView().getRootView(),"Write Permission needed to load and save images, please grant it in settings",Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SplashScreenActivity.this.finish();
                        }
                    }).show();
                }
                break;
            case MY_PERMISSIONS_REQUEST_DEVICE_CAMERA:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    handler.sendEmptyMessage(1);
                }
                else
                {
                    Snackbar.make(getWindow().getDecorView().getRootView(),"Camera Permission needed to take full advantage of app functionality, please grant it in settings",Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            handler.sendEmptyMessage(1);
                        }
                    }).show();
                }
        }
    }
}
