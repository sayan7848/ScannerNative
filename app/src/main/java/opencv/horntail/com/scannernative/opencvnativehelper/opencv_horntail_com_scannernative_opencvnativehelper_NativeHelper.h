/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper */

#ifndef _Included_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
#define _Included_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    test
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_test
  (JNIEnv *, jobject);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    bgr2rgb
 * Signature: (JJ)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_bgr2rgb
  (JNIEnv *, jobject, jlong, jlong);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    resize
 * Signature: (JJD)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_resize
  (JNIEnv *, jobject, jlong, jlong, jdouble);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    x2gray
 * Signature: (JJ)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_x2gray
  (JNIEnv *, jobject, jlong, jlong);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    x2rgb
 * Signature: (JJ)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_x2rgb
  (JNIEnv *, jobject, jlong, jlong);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    sobel
 * Signature: (JJI)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_sobel
  (JNIEnv *, jobject, jlong, jlong, jint);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    canny
 * Signature: (JJI)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_canny
  (JNIEnv *, jobject, jlong, jlong, jint);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    histEqual
 * Signature: (JJ)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_histEqual
  (JNIEnv *, jobject, jlong, jlong);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    drawHist
 * Signature: (JJ)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_drawHist
  (JNIEnv *, jobject, jlong, jlong);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    averageFilter
 * Signature: (JJI)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_averageFilter
  (JNIEnv *, jobject, jlong, jlong, jint);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    medianFilter
 * Signature: (JJI)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_medianFilter
  (JNIEnv *, jobject, jlong, jlong, jint);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    gaussianFilter
 * Signature: (JJI)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_gaussianFilter
  (JNIEnv *, jobject, jlong, jlong, jint);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    getScannedImage
 * Signature: (JJJJ)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_getScannedImage
  (JNIEnv *, jobject, jlong, jlong, jlong, jlong);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    x2MagicColor
 * Signature: (JJ)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_x2MagicColor
  (JNIEnv *, jobject, jlong, jlong);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    x2BW
 * Signature: (JJ)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_x2BW
  (JNIEnv *, jobject, jlong, jlong);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    houghLineTransform
 * Signature: (JJ)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_houghLineTransform
  (JNIEnv *, jobject, jlong, jlong);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    houghCircles
 * Signature: (JJ)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_houghCircles
  (JNIEnv *, jobject, jlong, jlong);

/*
 * Class:     opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper
 * Method:    rotate
 * Signature: (JJI)V
 */
JNIEXPORT void JNICALL Java_opencv_horntail_com_scannernative_opencvnativehelper_NativeHelper_rotate
  (JNIEnv *, jobject, jlong, jlong, jint);

#ifdef __cplusplus
}
#endif
#endif
