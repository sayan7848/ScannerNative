package opencv.horntail.com.scannernative.javahelper;

import android.util.Log;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import opencv.horntail.com.scannernative.opencvnativehelper.NativeHelper;


public class ImageConversionHelper {

    private  final String TAG = "ImageConversionHelper";

    public Mat x2gray(Mat sampledImage)
    {
        NativeHelper nativeHelper = new NativeHelper();
        try
        {
          
            Mat grayImage = new Mat(sampledImage.rows(),sampledImage.cols(), CvType.CV_8UC1);
            nativeHelper.x2gray(sampledImage.getNativeObjAddr(),grayImage.getNativeObjAddr());
           
            if(sampledImage!=null)
                sampledImage.release();
            sampledImage = null;
            
            return grayImage;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            return null;
        }
    }

    public Mat x2rgb(Mat sampledImage)
    {
        NativeHelper nativeHelper = new NativeHelper();
        try
        {
            Mat rgbImage = new Mat();
            nativeHelper.x2rgb(sampledImage.getNativeObjAddr(),rgbImage.getNativeObjAddr());
            if(sampledImage!=null)
                sampledImage.release();
            sampledImage = null;
            
            return rgbImage;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            return null;
        }
    }
}
