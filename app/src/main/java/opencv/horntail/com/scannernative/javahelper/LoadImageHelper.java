package opencv.horntail.com.scannernative.javahelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.File;
import java.io.IOException;

import opencv.horntail.com.scannernative.opencvnativehelper.NativeHelper;


public class LoadImageHelper {

    private Context context;
    private AppCompatActivity appCompatActivity;
    private NativeHelper nativeHelper;
    private int requiredWidth,requiredHeight;
    private final String TAG = "loadHelper";
    File photoFile;
    private String cameraPicPath;

    public LoadImageHelper(Context context, AppCompatActivity appCompatActivity, int requiredWidth,int requiredHeight) {
        this.context = context;
        this.appCompatActivity = appCompatActivity;
        this.requiredWidth = requiredWidth;
        this.requiredHeight = requiredHeight;
        nativeHelper = new NativeHelper();
    }

    public void loadImage(int requestCode) throws Exception {
        Intent intent;
        switch (requestCode) {
            case 1:
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                if (intent.resolveActivity(appCompatActivity.getPackageManager()) != null)
                    appCompatActivity.startActivityForResult(intent, requestCode);
                else
                    Toast.makeText(context, "no app to handle operation", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if (takePictureIntent.resolveActivity(appCompatActivity.getPackageManager()) != null) {

                    photoFile = null;
                    photoFile = createImageFile();

                    if (photoFile != null) {
                        Uri photoURI = Uri.fromFile(photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        appCompatActivity.startActivityForResult(takePictureIntent, requestCode);
                    }
                }
                break;
            default:
                break;
        }
    }

    public Mat onActivityResult(int requestCode, int resultCode, Intent data) {
        Mat sampledImage = null;
        switch (requestCode) {
            case 1:
                Log.e(TAG,"response recieved");
                try {
                    if (resultCode == Activity.RESULT_OK
                            && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = appCompatActivity.getContentResolver().query(selectedImage,
                                filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String imgDecodableString = cursor.getString(columnIndex);
                        Toast.makeText(context, imgDecodableString, Toast.LENGTH_SHORT).show();
                        cursor.close();
                        sampledImage = loadImageFromUri(imgDecodableString);

                    }
                } catch (Exception ex) {
                    Log.e(TAG,ex.getMessage());
                }
                break;
            case 2:
                Log.e(TAG,"response recieved");
                try
                {
                    if (resultCode == Activity.RESULT_OK) {
                        //String string = photoFile.getAbsolutePath();
                        sampledImage = loadImageFromUri(cameraPicPath).clone();
                        (new File(cameraPicPath)).delete();
                    }
                }
                catch (Exception ex)
                {
                    Log.e(TAG,ex.getMessage());
                }
                break;
            default:
                break;

        }
        return sampledImage;

    }

    private Mat loadImageFromUri(String path) {
        File file = new File(path);
        Log.e(TAG,path);
        if (!file.exists())
        {
            Log.e(TAG,"loadImage error");
            return null;
        }
        Mat scaledImage = new Mat();
        Mat bgrImage = Imgcodecs.imread(path);
        Mat rgbImage = new Mat();
        
        nativeHelper.bgr2rgb(bgrImage.getNativeObjAddr(),rgbImage.getNativeObjAddr());
        if(bgrImage!=null)
            bgrImage.release();
        bgrImage = null;
        int originalWidth = rgbImage.width();
        int originalHeight = rgbImage.height();
        Log.e("load", Integer.toString(requiredWidth)+" "+ Integer.toString(requiredHeight));
        double factor = calculateSampleSize(requiredWidth,requiredHeight, originalWidth, originalHeight);
        
        nativeHelper.resize(rgbImage.getNativeObjAddr(),scaledImage.getNativeObjAddr(),factor);
        if(rgbImage != null)
            rgbImage.release();
        rgbImage = null;
        return scaledImage;
    }

    private double calculateSampleSize(int requiredWidth, int requiredHeight, int originalWidth, int originalHeight) {
        double downScaleRatio = 1;
        if (originalHeight > requiredHeight || originalWidth > requiredWidth) {
            final double widthRatio = (double) requiredWidth / (double) originalWidth;
            final double heightRatio = (double) requiredHeight / (double) originalHeight;
            downScaleRatio = widthRatio < heightRatio ? widthRatio : heightRatio;
        }
        return downScaleRatio;
    }

    private File createImageFile() throws IOException {

       // File storageDir = appCompatActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/tie/tempPics");
        if(!storageDir.exists())
            storageDir.mkdirs();
       /* File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );*/
        cameraPicPath = Environment.getExternalStorageDirectory() + "/tie/tempPics/temp" + "_" + System.currentTimeMillis() + ".jpg";
        return new File(cameraPicPath);
    }

}
