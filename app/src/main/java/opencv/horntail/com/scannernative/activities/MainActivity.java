package opencv.horntail.com.scannernative.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import opencv.horntail.com.scannernative.R;
import opencv.horntail.com.scannernative.javahelper.DisplayImageHelper;
import opencv.horntail.com.scannernative.javahelper.EdgeDetectorHelper;
import opencv.horntail.com.scannernative.javahelper.HistogramHelper;
import opencv.horntail.com.scannernative.javahelper.HoughTransformHelper;
import opencv.horntail.com.scannernative.javahelper.ImageConversionHelper;
import opencv.horntail.com.scannernative.javahelper.ImageFilterHelper;
import opencv.horntail.com.scannernative.javahelper.LoadImageHelper;
import opencv.horntail.com.scannernative.javahelper.SaveImageHelper;
import opencv.horntail.com.scannernative.javahelper.ScanHelper;
import opencv.horntail.com.scannernative.opencvnativehelper.NativeHelper;
import opencv.horntail.com.scannernative.thirdpartyhelpers.PolygonView;
import opencv.horntail.com.scannernative.thirdpartyhelpers.TouchImageView;

public class MainActivity extends AppCompatActivity {

    private Mat scannedMat;
    private boolean showSave = true;
    private boolean applyOnLastEdited = true;
    private int spKernelSize;
    private final String TAG = "MainActivity";
    private Mat sampledImage,originalImage,alternateSampledImage,tempMat;
    private ProgressDialog progressDialog;
    private TouchImageView imageView;
    private DisplayImageHelper displayImageHelper;
    private ScanHelper scanHelper;
    private PolygonView polygonView;
    private boolean inScanFragment = false;
    private LoadImageHelper loadImageHelper;
    private AppCompatActivity appCompatActivity;


    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            boolean flag = false;
            switch(status) {
                case LoaderCallbackInterface.SUCCESS:
                    Log.i(TAG,"OpenCV Manager Connected");
                    System.loadLibrary("native");
                    flag = true;
                    break;
                case LoaderCallbackInterface.INIT_FAILED:
                    Log.i(TAG,"Init Failed");
                    flag = false;
                    break;
                case LoaderCallbackInterface.INSTALL_CANCELED:
                    Log.i(TAG,"Install Cancelled");
                    flag = false;
                    break;
                case LoaderCallbackInterface.INCOMPATIBLE_MANAGER_VERSION:
                    Log.i(TAG,"Incompatible Version");
                    flag = false;
                    break;
                case LoaderCallbackInterface.MARKET_ERROR:
                    Log.i(TAG,"Market Error");
                    flag = false;
                    break;
                default:
                    Log.i(TAG,"OpenCV Manager Install");
                    super.onManagerConnected(status);
                    flag = true;
                    break;
            }
            if(!flag)
            {
                makeToast("OpenCV Library Error, Exiting Application");
                MainActivity.this.finish();
            }
        }
    };

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.e(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
        } else {
            Log.e(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appCompatActivity = this;
        setContentView(R.layout.activity_main);
        imageView = (TouchImageView) findViewById(R.id.displayPanel);
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                createDialog(true);
                return false;
            }
        });
        polygonView = (PolygonView)findViewById(R.id.polygonView);
        displayImageHelper = new DisplayImageHelper();
        android.graphics.Point point = new android.graphics.Point();
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        int requiredWidth = point.x;
        int requiredHeight = point.y;
        loadImageHelper = new LoadImageHelper(this,this,requiredWidth,requiredHeight);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed()
    {
        createDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        inScanFragment = false;
        switch (menuItem.getItemId()) {
            case R.id.apply_on_last_edited:
                applyOnLastEdited = true;
                break;
            case R.id.undo:
                if(!checkValidity())
                {
                    break;
                }
                showSave = true;
                displayImageHelper.display(sampledImage,imageView);
                if(alternateSampledImage != null)
                    alternateSampledImage.release();
                alternateSampledImage = null;
                break;
            case R.id.apply_on_original:
                applyOnLastEdited = false;
                break;
            case R.id.load_from_cam:
                showSave = true;
                if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                {
                    makeToast("please grant camera permissions from settings");
                    break;
                }
                try {

                    loadImageHelper.loadImage(2);
                }
                catch (Exception ex)
                {
                    makeToast("could not load image");
                    Log.e(TAG,ex.getMessage());
                }
                break;
            case R.id.load_from_sto:
                showSave = true;
                try {

                    loadImageHelper.loadImage(1);
                }
                catch (Exception ex)
                {
                    makeToast("could not load image");
                    Log.e(TAG,ex.getMessage());
                }
                break;
            case R.id.scan:
                if(!checkValidity()){break;}
                tempMat = new Mat();
                if(applyOnLastEdited)
                    sampledImage.copyTo(tempMat);
                else
                    originalImage.copyTo(tempMat);
                scanHelper = new ScanHelper(polygonView,imageView,this,tempMat);
                if(scannedMat!=null)
                    scannedMat.release();
                scannedMat=null;
                scannedMat = tempMat.clone();
                inScanFragment = true;
                break;
            case R.id.scan_cancel:
                inScanFragment = false;
                try {
                    scanHelper.disablePolygonView(polygonView);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                displayImageHelper.display(sampledImage, imageView);
                break;
            case R.id.scan_done:
                inScanFragment = false;
                scanHelper.disablePolygonView(polygonView);
                if(scannedMat != null) {
                    if (sampledImage != null)
                        sampledImage.release();
                    sampledImage = new Mat();
                    originalImage = new Mat();
                    sampledImage = scannedMat.clone();
                }
                displayImageHelper.display(sampledImage,imageView);
                break;
            case R.id.avg_kernel_3:
                spKernelSize = 3;
                new commonAsynctask().execute(-1);
                break;
            case R.id.avg_kernel_7:
                spKernelSize = 7;
                new commonAsynctask().execute(-1);
                break;
            case R.id.avg_kernel_5:
                spKernelSize = 5;
                new commonAsynctask().execute(-1);
                break;
            case R.id.gaus_kernel_3:
                spKernelSize = 3;
                new commonAsynctask().execute(-2);
                break;
            case R.id.gaus_kernel_5:
                spKernelSize = 5;
                new commonAsynctask().execute(-2);
                break;
            case R.id.gaus_kernel_7:
                spKernelSize = 7;
                new commonAsynctask().execute(-2);
                break;
            case R.id.median_kernel_3:
                spKernelSize = 3;
                new commonAsynctask().execute(-3);
                break;
            case R.id.median_kernel_5:
                spKernelSize = 5;
                new commonAsynctask().execute(-3);
                break;
            case R.id.median_kernel_7:
                spKernelSize = 7;
                new commonAsynctask().execute(-3);
                break;
            case R.id.scan_start:
                new commonAsynctask().execute(R.id.scan_start);
                break;
            case R.id.scanGray:
                new commonAsynctask().execute(R.id.scanGray);
                break;
            case R.id.bw:
                new commonAsynctask().execute(R.id.bw);
                break;
            case R.id.magicColour:
                new commonAsynctask().execute(R.id.magicColour);
                break;
            case R.id.hist_equalized_image:
                new commonAsynctask().execute(R.id.hist_equalized_image);
                break;
            case R.id.image_histogram:
                new commonAsynctask().execute(R.id.image_histogram);
                break;
            case R.id.sobel:
                new commonAsynctask().execute(R.id.sobel);
                break;
            case R.id.canny:
                new commonAsynctask().execute(R.id.canny);
                break;
            case R.id.convert_to_gray:
                new commonAsynctask().execute(R.id.convert_to_gray);
                break;
            case R.id.convert_to_rgb:
                new commonAsynctask().execute(R.id.convert_to_rgb);
                break;
            case R.id.hough_line:
                new commonAsynctask().execute(R.id.hough_line);
                break;
            case R.id.hough_circle:
                new commonAsynctask().execute(R.id.hough_circle);
            default:
                break;

        }
        invalidateOptionsMenu();
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        menu.findItem(R.id.scan_start).setVisible(inScanFragment);
        menu.findItem(R.id.scan_done).setVisible(inScanFragment);
        menu.findItem(R.id.scan_cancel).setVisible(inScanFragment);
        menu.findItem(R.id.magicColour).setVisible(inScanFragment);
        menu.findItem(R.id.scanGray).setVisible(inScanFragment);
        menu.findItem(R.id.bw).setVisible(inScanFragment);
        menu.findItem(R.id.load).setVisible(!inScanFragment);
        menu.findItem(R.id.undo).setVisible(!showSave && !inScanFragment);
        menu.findItem(R.id.mode).setVisible(showSave && !inScanFragment);
        menu.findItem(R.id.extras).setVisible(showSave && !inScanFragment);
        menu.findItem(R.id.scan).setVisible(showSave && !inScanFragment);
        menu.findItem(R.id.medianFilter).setVisible(showSave && !inScanFragment);
        menu.findItem(R.id.gaussianFilter).setVisible(showSave && !inScanFragment);
        menu.findItem(R.id.averageFilter).setVisible(showSave && !inScanFragment);
        menu.findItem(R.id.hist_equalized_image).setVisible(showSave && !inScanFragment);
        menu.findItem(R.id.convert).setVisible(showSave && !inScanFragment);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(final int requestCode,final int resultCode,final Intent data)
    {
        super.onActivityResult(requestCode,resultCode,data);
        progressDialog = ProgressDialog.show(this,"Message","Loading...");
        new Thread()
        {
            @Override
            public void run()
            {
                Looper.prepare();
                sampledImage = loadImageHelper.onActivityResult(requestCode, resultCode, data);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
                if (sampledImage != null) {
                    originalImage = new Mat();
                    sampledImage.copyTo(originalImage);
                    showSave = true;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            displayImageHelper.display(sampledImage,imageView);
                        }
                    });
                }
                else
                {
                    makeToast("could not load image");
                    if(originalImage!=null)
                    {
                        sampledImage = new Mat();
                        originalImage.copyTo(sampledImage);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                displayImageHelper.display(sampledImage,imageView);
                            }
                        });
                    }
                }
            }
        }.start();
    }

    private void makeToast(final String toast)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),toast,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean checkValidity()
    {
        if(sampledImage == null || originalImage == null)
        {
            makeToast("select an image first");
            return false;
        }
        return true;
    }

    private void createDialog() {

        AlertDialog.Builder alertDlg = new AlertDialog.Builder(this);
        alertDlg.setMessage("Are you sure you want to exit?");
        alertDlg.setCancelable(false);
        alertDlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.this.finish();
                    }
                }
        );
        alertDlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDlg.create().show();
    }

    private void createDialog(boolean flag)
    {
        if(sampledImage != null && originalImage != null) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("TIE").setItems(R.array.save, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            Bitmap bm = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                            progressDialog = ProgressDialog.show(appCompatActivity, "TIE", "Saving...");
                            final boolean status = new SaveImageHelper().saveImage(bm,appCompatActivity);
                            new Thread() {
                                @Override
                                public void run() {
                                    Looper.prepare();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressDialog.dismiss();
                                            if (status) {
                                                makeToast("Image saved at " + Environment.getExternalStorageDirectory()+"/tie/savedPics");
                                            } else {
                                                makeToast("Image could not be saved");
                                            }
                                        }
                                    });
                                }
                            }.start();
                            break;
                        case 1:
                            new commonAsynctask().execute(-4);
                            break;
                        case 2:
                            new commonAsynctask().execute(-5);
                            break;
                        default:
                            break;
                    }
                }
            });
            alertDialog.create().show();
        }
    }

    private class commonAsynctask extends AsyncTask<Integer,Void,Mat>
    {
        @Override
        public void onPreExecute()
        {
            if(!checkValidity()) {
                cancel(true);
                return;
            }
            if(applyOnLastEdited)
            {
                tempMat = sampledImage.clone();
            }
            else
            {
                tempMat = originalImage.clone();
            }
            progressDialog = ProgressDialog.show(appCompatActivity,"TIE","Executing Task...");
        }

        @Override
        public Mat doInBackground(Integer...integers)
        {
            switch (integers[0])
            {
                case R.id.hough_circle:
                    showSave = false;
                    if(alternateSampledImage != null)
                        alternateSampledImage.release();
                    alternateSampledImage = (new HoughTransformHelper().circleTransform(tempMat)).clone();
                    return alternateSampledImage;
                case R.id.hough_line:
                    showSave = false;
                    if(alternateSampledImage != null)
                        alternateSampledImage.release();
                    alternateSampledImage = (new HoughTransformHelper().circleTransform(tempMat)).clone();
                    return alternateSampledImage;
                case R.id.image_histogram:
                    showSave = false;
                    if(alternateSampledImage != null)
                        alternateSampledImage.release();
                    alternateSampledImage = (new HistogramHelper().drawHist(tempMat)).clone();
                    return alternateSampledImage;
                case R.id.canny:
                    showSave = false;
                    if(alternateSampledImage != null)
                        alternateSampledImage.release();
                    alternateSampledImage = (new EdgeDetectorHelper().Canny(tempMat,3)).clone();
                    return alternateSampledImage;
                case R.id.sobel:
                    showSave = false;
                    if(alternateSampledImage != null)
                        alternateSampledImage.release();
                    alternateSampledImage = (new EdgeDetectorHelper().Sobel(tempMat,3)).clone();
                    return alternateSampledImage;
                case R.id.hist_equalized_image:
                    if(sampledImage != null)
                        sampledImage.release();
                    sampledImage = (new HistogramHelper().histEqualize(tempMat)).clone();
                    return sampledImage;
                case R.id.convert_to_gray:
                    if(sampledImage != null)
                        sampledImage.release();
                    sampledImage = (new ImageConversionHelper().x2gray(tempMat)).clone();
                    return sampledImage;
                case R.id.convert_to_rgb:
                    if(sampledImage != null)
                        sampledImage.release();
                    sampledImage = (new ImageConversionHelper().x2rgb(tempMat)).clone();
                    return sampledImage;
                case R.id.scanGray:
                    inScanFragment = true;
                    scannedMat = scanHelper.getGray(scannedMat).clone();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            polygonView.setVisibility(View.GONE);
                        }
                    });
                    return scannedMat;
                case R.id.magicColour:
                    inScanFragment = true;
                    scannedMat = scanHelper.getMagicColour(scannedMat).clone();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            polygonView.setVisibility(View.GONE);
                        }
                    });
                    return scannedMat;
                case R.id.bw:
                    inScanFragment = true;
                    scannedMat = scanHelper.getBW(scannedMat).clone();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            polygonView.setVisibility(View.GONE);
                        }
                    });
                    return scannedMat;
                case R.id.scan_start:
                    inScanFragment = true;
                    Mat holder = scanHelper.startScan(polygonView);
                    if(holder == null)
                    {
                        Looper.prepare();
                        makeToast("invalid crop shape");
                        cancel(true);
                    }
                    else
                    {
                        if(scannedMat != null)
                            scannedMat.release();
                        scannedMat = holder.clone();
                        holder.release();
                        holder = null;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                polygonView.setVisibility(View.GONE);
                            }
                        });
                        return scannedMat;
                    }
                    return null;
                case -1:
                    if(sampledImage != null)
                        sampledImage.release();
                    sampledImage = (new ImageFilterHelper().averageFilter(tempMat,spKernelSize)).clone();
                    return sampledImage;
                case -2:
                    if(sampledImage != null)
                        sampledImage.release();
                    sampledImage = (new ImageFilterHelper().gaussianFilter(tempMat,spKernelSize)).clone();
                    return sampledImage;
                case -3:
                    if(sampledImage != null)
                        sampledImage.release();
                    sampledImage = (new ImageFilterHelper().medianFilter(tempMat,spKernelSize)).clone();
                    return sampledImage;
                case -4:
                    new NativeHelper().rotate(sampledImage.getNativeObjAddr(),sampledImage.getNativeObjAddr(),0);
                    return sampledImage;
                case -5:
                    new NativeHelper().rotate(sampledImage.getNativeObjAddr(),sampledImage.getNativeObjAddr(),1);
                    return sampledImage;
                default:
                    break;
            }
            return null;
        }

        @Override
        public void onPostExecute(Mat mat)
        {
            try
            {
                progressDialog.dismiss();
                if(tempMat != null)
                    tempMat.release();
                tempMat = null;
                if(mat != null)
                    displayImageHelper.display(mat,imageView);
            }
            catch (Exception ex)
            {
                Looper.prepare();
                makeToast("could not do operation");
                ex.printStackTrace();
            }
            invalidateOptionsMenu();
        }

        @Override
        public void onCancelled()
        {
            try
            {
                progressDialog.dismiss();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            invalidateOptionsMenu();
            super.onCancelled();
        }
    }
}
