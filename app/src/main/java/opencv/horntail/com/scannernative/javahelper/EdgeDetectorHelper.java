package opencv.horntail.com.scannernative.javahelper;

import android.util.Log;

import org.opencv.core.Mat;

import opencv.horntail.com.scannernative.opencvnativehelper.NativeHelper;


public class EdgeDetectorHelper {

    private  final String TAG = "EdgeDetector";

    public Mat Sobel(Mat image, int kernelSize)
    {
        NativeHelper nativeHelper = new NativeHelper();
        try {
            Mat edgeImage = new Mat();
            nativeHelper.sobel(image.getNativeObjAddr(), edgeImage.getNativeObjAddr(), kernelSize);
            return edgeImage;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            return null;
        }
    }
    public Mat Canny(Mat image, int kernelSize)
    {
        NativeHelper nativeHelper = new NativeHelper();
        try
        {
            Mat edgeImage = new Mat();
            nativeHelper.canny(image.getNativeObjAddr(),edgeImage.getNativeObjAddr(),kernelSize);
            return edgeImage;
        }
        catch (Exception ex)
        {
            Log.e(TAG,ex.getMessage());
            return null;
        }
    }
}
