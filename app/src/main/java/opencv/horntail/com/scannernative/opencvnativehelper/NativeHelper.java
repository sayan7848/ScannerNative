package opencv.horntail.com.scannernative.opencvnativehelper;


public class NativeHelper {

    public native void test();
    public native void bgr2rgb(long Matptr1,long Matptr2);
    public native void resize(long Matptr1,long Matptr2,double factor);
    public native void x2gray(long Matptr1,long Matptr2);
    public native void x2rgb(long Matptr1,long Matptr2);
    public native void sobel(long Matptr1,long Matptr2,int kernelSize);
    public native void canny(long Matptr1,long Matptr2,int kernelSize);
    public native void histEqual(long Matptr1,long Matptr2);
    public native void drawHist(long Matptr1,long Matptr2);
    public native void averageFilter(long Matptr1,long Matptr2,int kernelSize);
    public native void medianFilter(long Matptr1,long Matptr2,int kernelSize);
    public native void gaussianFilter(long Matptr1,long Matptr2,int kernelSize);
    public native void getScannedImage(long sampledImagePtr,long correctedImagePtr,long sourcePoints,long destPoints);
    public native void x2MagicColor(long Matptr1,long Matptr2);
    public native void x2BW(long Matptr1,long Matptr2);
    public native void houghLineTransform(long Matptr1,long Matptr2);
    public native void houghCircles(long Matptr1,long Matptr2);
    public native void rotate(long Matptr1,long Matptr2,int code);
}
