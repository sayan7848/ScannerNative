# ScannerNative
Demonstrates some basic computer vision algorithms using opencv library
## This App Can
Load Image from both device storage and camera </br>
Apply mean,median,gaussian filters <br />
Convert image to rgb/grayscale <br />
Equalize Histogram <br />
Give a scanned effect to image (doc mode) using perspective correction and adaptive thresholding <br />
Save edited image to local storage <br />
Show Image Histogram <br />
Show Edge Image (Canny and Sobel methods) <br />
###External libraries used
OpenCV <br />
PolygonView from ScanLibrary of Jhansi Reddy <br />
TouchImageView of Mike Ortitz and team <br />
